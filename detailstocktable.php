

<?php
//auto
// $q_column = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='smart' AND `TABLE_NAME`='oshe'";
// $d_columns = $db->rawQuery($q_column);
//end of auto
$item = isset($_GET['item']) ? $_GET['item'] : ""; 
$table = "stock_log";
// $txt_field= "user_name,user_nama,user_hp,user_email,user_tipe,user_foto";
// $txt_label = "Username,Nama,HP,Email,Tipe,Foto";
$txt_field= "
slType
,slRemark
,slPrice
,slQty
,slDate
,slUsername
";

$txt_label = "
Type
,Remark
,Price
,QTY
,Date
,User

";
$q_field = explode(",",$txt_field);
$q_label = explode(",",$txt_label);
// $i=1;$q_oshe = "select ".$q_field[0] ." as " .$q_label[0];
// for($i;$i<count($q_field);$i++)
// {
//     $q_oshe .= ",".$q_field[$i] ." as " .$q_label[$i];
// }
// $q_oshe .= " from $table";
// $d_oshe = $db->rawQuery($q_oshe);
?>
 <style>
  div.dataTables_wrapper {
        width: 800px;
        margin: 0 auto;
    }
    .centerleft {
      text-align : center;
      left : 20%;
    }
    .center {
      text-align : center;
    }
    </style>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

  <link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="dist/css/jquery.datetimepicker.css">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/jszip-2.5.0/dt-1.10.21/b-1.6.3/b-html5-1.6.3/fh-3.1.7/r-2.2.5/sp-1.1.1/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/jszip-2.5.0/dt-1.10.21/b-1.6.3/b-html5-1.6.3/fh-3.1.7/r-2.2.5/sp-1.1.1/datatables.min.js"></script>
<script src="dist/js/jquery.datetimepicker.js"></script>


          <div class="card">
            <div class="card-header">
              <h3 class="card-title center">Item Name : <?=$item?></h3>
              <!-- <button onclick="" type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default">
                  Absensi
                </button> -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            
            <div class="col-md-6 centerleft">
                            <div class="form-group">
                                <label>Tarikh:</label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="far fa-calendar-alt"></i>
                                    </span>
                                  </div>
                                  
                                  <input value="<?=(new \DateTime())->format('Y-m-d')?>"  name="tgl"  type="text" class="form-control float-right" id="tgl">
                                  <span class="input-group-append">
                                  <button type="button" id="btnpilih" class="btn btn-primary">Pilih</button>
                                  <button type="button" id="btnreset" onclick="resettgl();" class="btn btn-danger">Reset</button>
                                  </span>
                                </div>
                                <!-- <input type="text" class="form-control" placeholder="TARIKH" value=""> -->
                            </div>
                          </div>
                          <div class="col-md-6 centerleft">
                          <a href="stocktable.php" class="btn btn-secondary">Back</a>
                          </div>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                <th>No</th>
                  <?php
                    foreach ($q_label as $key => $value) {
                      echo "<th>".$value."</th>";
                      // var_dump($value);
                    }
  
                  ?>
                </tr>
                </thead>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->



 

      <script>
 function resettgl()
      { document.getElementById('tgl').value=""; }
      
      $(document).ready( function () {
    // $('#example2').DataTable();

      var tabel = $('#example2').DataTable({
      "orderCellsTop": true,
      "fixedHeader": true,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "scrollX": true,
    //   "scrollY": "500px",
      "scrollCollapse": true,
      // "dom": 'Bfrtip',

      "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "ajax": "getDataStockDetail.php?mode=list&item=<?=$item?>"
        
    }); //end of datatables


    $('#tgl').datetimepicker({
      timepicker:false,
    //   format:'d-m-Y',
      format:'Y-m-d',
	formatDate:'Y/m/d',
  defaultDate: new Date() 
	// minDate:'-1970/01/02', // yesterday is minimum date
    });

    $('#btnpilih').click( function() {
    // console.log("onchange");
    var tgl = $('#tgl').val();
    // var tgl= new Date(d);  // getting search input value
    // var datestring = tgl.getFullYear()  + "-" + (tgl.getMonth()+1) + "-" + tgl.getDate();
    tabel.columns(2).search(tgl).draw();
    } );
    // document.getElementById('btnpilih').click();
} );
      </script>