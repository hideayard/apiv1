<?php

$table = "transaction";
$txt_field= "
ts_no
,ts_date
,tsCreatedByUsername
,ts_price
";

$txt_label = "
No
,Date
,Employee
,Price

";
$q_field = explode(",",$txt_field);
$q_label = explode(",",$txt_label);

?>



<div class="wrapper">



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6 center">
            <h1>SALES REPORT</h1>
          </div>

        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <!-- <h3 class="card-title">List Data oshe</h3> -->

            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <div class="col-md-6 center">
                            <div class="form-group ">
                                <label>Tarikh:</label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="far fa-calendar-alt"></i>
                                    </span>
                                  </div>
                                  <input value="<?=(new \DateTime())->format('Y-m-d')?>"  name="tgl"  type="text" class="form-control float-right" id="tgl">
                                  <span class="input-group-append">
                                  <button type="button" id="btnpilih" class="btn btn-primary">Pilih</button>
                                  <button type="button" id="btnreset" onclick="resettgl();" class="btn btn-secondary">Reset</button>
                                  </span>
                                </div>
                                <!-- <input type="text" class="form-control" placeholder="TARIKH" value=""> -->
                            </div>
                          </div>

              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <?php
                    foreach ($q_label as $key => $value) {
                      echo "<th>".$value."</th>";
                      // var_dump($value);
                    }
  
                  ?>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
                <!-- <tfoot>
                <tr> -->
                <?php
                    // foreach ($q_label as $key => $value) {
                    //   echo "<th>".$value."</th>";
                    // }
                  ?>
                <!-- </tr>
                </tfoot> -->
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
    <script>

      function resettgl()
      { document.getElementById('tgl').value=""; }
      
      $(document).ready( function () {
    // $('#example2').DataTable();

      var tabel = $('#example2').DataTable({
      "orderCellsTop": true,
      "fixedHeader": true,
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "scrollX": true,
      "scrollY": "500px",
      "scrollCollapse": true,
      // "dom": 'Bfrtip',
      "order": [[ 3, "desc" ]],

      "columnDefs": [ {"searchable": false,"orderable": false,"targets": 0 } 
                      ,{"searchable": false,"orderable": false,"targets": 1 } ],
        "ajax": "get_data_sales.php?mode=list"
        
    }); //end of datatables


    $('#tgl').datetimepicker({
      timepicker:false,
    //   format:'d-m-Y',
      format:'Y-m-d',
	formatDate:'Y/m/d',
  defaultDate: new Date() 
	// minDate:'-1970/01/02', // yesterday is minimum date
    });

    $('#btnpilih').click( function() {
    // console.log("onchange");
    var tgl = $('#tgl').val();
    // var tgl= new Date(d);  // getting search input value
    // var datestring = tgl.getFullYear()  + "-" + (tgl.getMonth()+1) + "-" + tgl.getDate();
    tabel.columns(3).search(tgl).draw();
    } );
    // document.getElementById('btnpilih').click();
} );
      </script>