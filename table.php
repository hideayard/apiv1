<?php
session_start();
// error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('config/MysqliDb.php');
include_once ("config/db.php");
$db = new MysqliDb ('localhost', $dbuser, $dbpass, $dbname);
include("config/functions.php");

date_default_timezone_set("Asia/Jakarta");
$tgl=date('Y-m-d');
$skrg = date("Y-m-d h:i:sa");
$page=isset($_GET['page']) ? $_GET['page'] : "sales"; 
    
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OSH PRO | Dashboard</title>
  <link rel="icon" href="assets/img/logo.png" type="image/png" sizes="50x50">  <!-- Tell the browser to be responsive to screen width -->
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
 

  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  
  <style>
  .center{text-align:center;}
  </style>
  


<style>
.photo_preview{
    /* display:none; */
    width : 200px;
    height : 200px;
}
.dropzone-previews {
    /* height: 100px; */
    width: 100%;
    border: dashed 1px blue;
    background-color: lightblue;
}
    </style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="dist/css/jquery.datetimepicker.css">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/jszip-2.5.0/dt-1.10.21/b-1.6.3/b-html5-1.6.3/fh-3.1.7/r-2.2.5/sp-1.1.1/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/jszip-2.5.0/dt-1.10.21/b-1.6.3/b-html5-1.6.3/fh-3.1.7/r-2.2.5/sp-1.1.1/datatables.min.js"></script>
<script src="dist/js/jquery.datetimepicker.js"></script>


</head>
<body class="hold-transition layout-top-nav">

<!-- <header class="entry-header">
  <h1 class="entry-title">OSH PRO</h1>
</header> -->

<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<div class="wrapper">



<?php //require_once ("sidebar.php"); ?>

  <?php
//   if(!check_role($page,null) && ($page!="home") && ($page!="logout") && ($page!="login") && ( (strpos($page, 'vz') !== false) < 0 ) && ($page!="profile") )
//   {
//     echo "<script>Swal.fire(
//                       'Info!',
//                       'You are not authorized!',
//                       'info'
//                       );
//                 console.log('You Are Not Authorized');
//                 setTimeout(function(){ window.stop();window.location='home'; }, 1000);
                
//                 </script>";
//       // echo json_encode( array("status" => false,"info" => "You are not authorized.!!!","messages" => "You are not authorized.!!!" ) );
//     // echo "<script>alert('You are not permitted!!!');window.location='home';</script>";
//   }
//   else
// include("sales.php");
  {
  // check_role($page,'');
            // $users = $db->get('users', 10); //contains an Array 10 users
            //print_r($users);
        //     if((($tipe_user=="MAINTENANCE") && (strpos($page, 'ucux') !== false) ) || ($page=="logout")  || ($page=="profile")   )
        //     {
        //       // include("error.php");
        //       // echo $tipe_user . "=". (strpos($page, 'ucux') !== false) ;
              if (file_exists("".$page.".php")) 
              {
                  include("".$page.".php");
              }
              else 
              {
                  include("error.php");
              }

        //     }
        //     else if( ( $tipe_user=="MAINTENANCE") &&  ($page=="home")   )
        //     {
        //           include("ucux.php");
        //     }
        //     else if($tipe_user!="MAINTENANCE")
        //     {
            
        //     if (file_exists("".$page.".php")) 
        //     {
        //         include("".$page.".php");
        //     }
        //     else 
        //     {
        //         include("error.php");
        //     }
        //   }

        }
          ?>

  <footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="#">M4D Studio</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.1
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery UI 1.11.4 -->
<!-- <script src="plugins/jquery-ui/jquery-ui.min.js"></script> -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  // $.widget.bridge('uibutton', $.ui.button)
</script>

<!-- Sparkline -->
<!-- <script src="plugins/sparklines/sparkline.js"></script> -->
<!-- JQVMap -->
<!-- <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script> -->
<!-- jQuery Knob Chart -->
<!-- <script src="plugins/jquery-knob/jquery.knob.min.js"></script> -->
<!-- <script src="dist/js/jquery.datetimepicker.js"></script> -->
<!-- jquery.datetimepicker.css -->
<!-- daterangepicker -->
<!-- <script src="plugins/moment/moment.min.js"></script> -->
<!-- <script src="plugins/daterangepicker/daterangepicker.js"></script> -->
<!-- Tempusdominus Bootstrap 4 -->
<!-- <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script> -->
<!-- Summernote -->
<!-- <script src="plugins/summernote/summernote-bs4.min.js"></script> -->
<!-- overlayScrollbars -->
<!-- <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script> -->
<!-- AdminLTE App -->
<!-- <script src="dist/js/adminlte.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="dist/js/demo.js"></script> -->



<!-- Tempusdominus Bootstrap 4 -->
<!-- <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script> -->
<!-- datepicker -->
<!-- <script src="dist/js/bootstrap-datepicker.min.js"></script> -->
<!-- date-range-picker -->
<!-- <script src="plugins/daterangepicker/daterangepicker.js"></script> -->
<!-- Select2 -->
<!-- <script src="plugins/select2/js/select2.full.min.js"></script> -->
<?php //if(($page=="role") || ($page=="users") || ($page=="program") || (strpos($page, 'table') !== false)  || (strpos($page, 'report') !== false)) {   ?>

<!-- DataTables -->
<!-- <script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="dist/js/dataTables.buttons.min.js"></script> -->

<!-- AdminLTE App -->
<!-- <script src="dist/js/buttons.flash.min.js"></script>
<script src="dist/js/pdfmake.min.js"></script>
<script src="dist/js/buttons.html5.min.js"></script>
<script src="dist/js/buttons.print.min.js"></script>
<script src="dist/js/jszip.min.js"></script>
<script src="dist/js/buttons.colVis.min.js"></script>
<script src="dist/js/buttons.bootstrap4.min.js"></script> -->
<?php //} ?>



<!-- toggle -->
<!-- <script src="plugins/bootstrap-toggle-master/js/bootstrap-toggle.js"></script> -->
<style type="text/css">/* Chart.js */
@keyframes chartjs-render-animation{from{opacity:.99}to{opacity:1}}.chartjs-render-monitor{animation:chartjs-render-animation 1ms}.chartjs-size-monitor,.chartjs-size-monitor-expand,.chartjs-size-monitor-shrink{position:absolute;direction:ltr;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1}.chartjs-size-monitor-expand>div{position:absolute;width:1000000px;height:1000000px;left:0;top:0}.chartjs-size-monitor-shrink>div{position:absolute;width:200%;height:200%;left:0;top:0}</style>
<script>
  
  $(document).ready(function() {
	
	setTimeout(function(){
		$('body').addClass('loaded');
		// $('h1').css('color','#222222');
	}, 1000);
	
});

</script>


<script type="text/javascript" src="dist/js/jquery-barcode.js"></script>

    <!-- <script src="dist/js/vfs_fonts.js"></script> -->

</body>
</html>
