<?php
require('./bootstrap.php');

use \Firebase\JWT\JWT;

// $privateKey = file_get_contents('mykey.pem');
$privateKey = "-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQDyI0dIrdCk/Hz5mF6IQh+L/9ftdgOVPUP2wTLeNX7o/aZCAibG
AmC5//ZuYL0sQmN4e1IpthPcHocIIvUgRx3CgTfhpy0/XeH1XeVkogbavtNQ6C05
KvudbgR9gKVz2Xvqm3a3lGMA7Uy6jVs01tiX5BTDdZ86d1p/bd7RY4ifmwIDAQAB
AoGAAgKI9PF8aeIup2O8ZSUnXeIhyLbSEMsRZVr1MSMAiq6QUrvx1nLumpylTwU+
jL+AU9+yLuZ0A7+9HzQrFLmCZbbKrde95mLTZOTJLvUW1Kwb+JxG16hG32YjOZfj
opiWsoaLZf8o6ivX/MB7c2rNRFv1UOiqbjryV4XcqNJM6wECQQD7s4JWqKVPAp7D
oQLyaufVXIYOEO4RNVUrlk4z3vAbx6hD9Kqsg/qWHqCa91EAi3wS/DZbw+MDjmce
bbwM3xq1AkEA9kX0w3ZsikUeDBz5FkwPpWWDNLiPR6SNbl9AqG7BwNw0lLhiSkQL
oXVZudL/AyfbPXvoZ1gYr8NnRfGz8hQzDwJAadbUCeikipLJe+2qtZEqp1der2+D
DA1nIQr1G261fJj6Mokhc6WTq6h5VHCGyTneFIWlvfSi8w3gYrU2DBuMuQJAJPYi
4iMn7yioddDzYXFQ99Xgd00r0+jSgToyjhTxZ2ylR5zUw6iqxjTCnq0YMPUOZo/u
NXR3YAnHuiuogjCd6QJBAIEc7nGuQS9XMycj7ht0FEi1ZnvlLazji6N8rrNC6F8L
R+O4hwlho/wb7W0ydYhjV42tiaUFNGkJ93wzIGVKBlU=
-----END RSA PRIVATE KEY-----";
$token = array(
    "iss" => "http://example.org",
    "aud" => "http://example.com",
    "iat" => 1356999524,
    "nbf" => 1357000000
);

/**
 * IMPORTANT:
 * You must specify supported algorithms for your application. See
 * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
 * for a list of spec-compliant algorithms.
 */
$jwt = JWT::encode($token, $privateKey, 'RS256');

echo $jwt . "\n";